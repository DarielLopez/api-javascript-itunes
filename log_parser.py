import re
from collections import Counter


# en esta funcion se puede buscar los ips y el numero de peticiones
def escanear(filename):
     with open(filename) as f:
          log= f.read()
          regexp= r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}'
          ips_list= re.findall(regexp,log)
          
          return  Counter(ips_list)
print("Listado de Ips\n")
print(escanear('listadohttp.log'))
print("\n")


# esta funcion es para ver las peticiones por codigos de respuesta.
def codi(filename):
     with open(filename) as f:
          log= f.read()
          code= r'" (200)'
          code2= r'" (404)'
          code3= r'" (503)'
          code4= r'" (500)'

          code= re.findall(code,log)
          code2=re.findall(code2,log)
          code3=re.findall(code3,log)
          code4=re.findall(code4,log)
          a=Counter(code)
          b=Counter(code2)
          c=Counter(code3)
          d=Counter(code4)   
          
              
          return a+b+c+d





print("Listado de codigos de respuesta\n")
print(codi('listadohttp.log'))























